.PHONY: all, clean

all: target.pdf

target.pdf: 
	OUT_NAME=`latexmk -pdf | sed "s/^Latexmk: All targets (\(.*\)) are up-to-date/\1/g" | cat | tail -n 1`; mv $$OUT_NAME target.pdf
clean: 
	-latexmk -C paper.tex
	-rm *-eps-converted-to.pdf
	-rm target.pdf
